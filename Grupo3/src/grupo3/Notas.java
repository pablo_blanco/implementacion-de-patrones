/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grupo3;

import java.util.ArrayList;

/**
 *
 * @author p_ab1
 */
public abstract class Notas {
    private ArrayList<PadreObservador> observadores = new ArrayList<PadreObservador>();

    public Notas() {
    }

     public void agregarObservador(PadreObservador o)
    {
        observadores.add(o);
         notificarObservadores();
    }
     
     public void eliminarObservador(PadreObservador o) {
        observadores.remove(o);
    }
     
    public void notificarObservadores()
    {
         for (PadreObservador obj : observadores) {
             obj.observadoActualizado();
        }
    }
}
