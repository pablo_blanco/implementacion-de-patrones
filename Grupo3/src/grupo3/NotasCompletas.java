/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grupo3;

/**
 *
 * @author p_ab1
 */
public class NotasCompletas extends Notas{
    private int nota = 0;
    public NotasCompletas() {
    }

    public void setNota(int nota) {
        this.nota = nota;
        this.notificarObservadores();
    }

    public int getNota() {
        return nota;
    }
    
}
