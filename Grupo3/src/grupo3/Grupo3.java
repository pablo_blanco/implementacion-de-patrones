/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grupo3;

/**
 *
 * @author p_ab1
 */
public class Grupo3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Instanciar el objeto que será Observado
        NotasCompletas notas = new NotasCompletas();
         // Instanciar y registrar un Observador
         Padre objObservadorPepe  = new Padre(0,"Matematica","Pepe");
         notas.agregarObservador(objObservadorPepe);
         
         // Instanciar y registrar otro Observador
        Padre objObservadorJuan  = new Padre(0,"Matematicas","Juan");
        notas.agregarObservador(objObservadorJuan);
        
        // Instanciar y registrar otro Observador
        Padre objObservadorPablo  = new Padre(0,"Matematicas","Pablo");
        notas.agregarObservador(objObservadorPablo);
        System.out.println("------------------------------------------------------------");

        
    }
    
}
