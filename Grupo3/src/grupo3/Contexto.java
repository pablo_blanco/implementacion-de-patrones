/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grupo3;

/**
 *
 * @author p_ab1
 */
public class Contexto {
    private EstadoNota estado;
    private Alumno alumno;

    public Contexto(EstadoNota estado, Alumno alumno) {
        this.estado = estado;
        this.alumno = alumno;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public EstadoNota getEstado() {
        return estado;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public void setEstado(EstadoNota estado) {
        this.estado = estado;
    }
    
    public void request()
    {
        estado.Estado(this.alumno);
    }
    
}
