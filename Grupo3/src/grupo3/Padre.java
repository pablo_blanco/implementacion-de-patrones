/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grupo3;

/**
 *
 * @author p_ab1
 */
public class Padre implements PadreObservador{
    
    private int nota;
    private String clase;
    private String nombre;

    public Padre(int nota, String clase,String nombre) {
        this.nota = nota;
        this.clase = clase;
        this.nombre = nombre;
        System.out.println("Padre " + this.nombre + " creado");

    }
    
    
    @Override
    public void observadoActualizado() {
        System.out.println("Padre " + this.nombre + " recibe la notificación");
    }
    
}
